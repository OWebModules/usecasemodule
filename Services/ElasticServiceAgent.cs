﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UseCaseModule.Models;

namespace UseCaseModule.Services
{
    public class ElasticServiceAgent
    {
        private Globals globals;

        public async Task<ResultItem<GetUseCaseResourcesResult>> GetuseCaseResources(List<UseCase> useCases)
        {
            var taskResult = await Task.Run<ResultItem<GetUseCaseResourcesResult>>(() =>
           {
               var result = new ResultItem<GetUseCaseResourcesResult>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new GetUseCaseResourcesResult()
               };

               if (!useCases.Any())
               {
                   return result;
               }

               result.Result.UseCases = useCases;

               var dbReaderActors = new OntologyModDBConnector(globals);

               var searchIds = useCases.Select(useCase => new clsObjectRel
               {
                   ID_Object = useCase.IdUseCase,
                   ID_RelationType = Config.LocalData.ClassRel_Use_Case_identified_by_Use_Case_ID.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Use_Case_identified_by_Use_Case_ID.ID_Class_Right
               }).ToList();

               var dbReaderIds = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderIds.GetDataObjectRel(searchIds);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Ids!";
                   return result;
               }

               result.Result.Ids = dbReaderIds.ObjectRels;

               var searchDescriptions = useCases.Select(useCase => new clsObjectRel
               {
                   ID_Object = useCase.IdUseCase,
                   ID_RelationType = Config.LocalData.ClassRel_Use_Case_is_described_by_Description__Use_Case_.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Use_Case_is_described_by_Description__Use_Case_.ID_Class_Right
               }).ToList();

               var dbReaderDescription = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderDescription.GetDataObjectRel(searchDescriptions);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Descriptions!";
                   return result;
               }

               result.Result.Descriptions = dbReaderDescription.ObjectRels;

               var searchActors = useCases.Select(useCase => new clsObjectRel
               {
                   ID_Object = useCase.IdUseCase,
                   ID_RelationType = Config.LocalData.ClassRel_Use_Case_Primary_Actor.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Use_Case_Primary_Actor.ID_Class_Right
               }).ToList();

               searchActors.AddRange(useCases.Select(useCase => new clsObjectRel
               {
                   ID_Object = useCase.IdUseCase,
                   ID_RelationType = Config.LocalData.ClassRel_Use_Case_Secondary_Actor.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Use_Case_Secondary_Actor.ID_Class_Right
               }));

               result.ResultState = dbReaderActors.GetDataObjectRel(searchActors);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Actors!";
                   return result;
               }

               result.Result.Actors = dbReaderActors.ObjectRels;


               var dbReaderConditions = new OntologyModDBConnector(globals);

               var searchConditions = useCases.Select(useCase => new clsObjectRel
               {
                   ID_Object = useCase.IdUseCase,
                   ID_RelationType = Config.LocalData.ClassRel_Use_Case_Pre_Conditions.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Use_Case_Pre_Conditions.ID_Class_Right
               }).ToList();

               searchConditions.AddRange(useCases.Select(useCase => new clsObjectRel
               {
                   ID_Object = useCase.IdUseCase,
                   ID_RelationType = Config.LocalData.ClassRel_Use_Case_Posts_Conditions.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Use_Case_Posts_Conditions.ID_Class_Right
               }));

               result.ResultState = dbReaderConditions.GetDataObjectRel(searchConditions);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Conditions!";
                   return result;
               }

               result.Result.Conditions = dbReaderConditions.ObjectRels;


               var dbReaderScenes = new OntologyModDBConnector(globals);

               var searchScenes = useCases.Select(useCase => new clsObjectRel
               {
                   ID_Object = useCase.IdUseCase,
                   ID_RelationType = Config.LocalData.ClassRel_Use_Case_Main_Scenes.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Use_Case_Main_Scenes.ID_Class_Right
               }).ToList();

               searchScenes.AddRange(useCases.Select(useCase => new clsObjectRel
               {
                   ID_Object = useCase.IdUseCase,
                   ID_RelationType = Config.LocalData.ClassRel_Use_Case_additional_Scenes.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Use_Case_additional_Scenes.ID_Class_Right
               }));

               result.ResultState = dbReaderScenes.GetDataObjectRel(searchScenes);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Scenes!";
                   return result;
               }

               result.Result.Scenes = dbReaderScenes.ObjectRels;

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<List<clsObjectRel>>> GetScenesTree(List<clsOntologyItem> scenes)
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectRel>>>(() =>
           {
               var result = new ResultItem<List<clsObjectRel>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new List<clsObjectRel>()
               };

               if (!scenes.Any())
               {
                   return result;
               }

               var dbReader = new OntologyModDBConnector(globals);
               var searchSubScenes = scenes.Select(scene => new clsObjectRel
               {
                   ID_Object = scene.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_Scenes_contains_Scenes.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Scenes_contains_Scenes.ID_Class_Right
               }).ToList();

               result.ResultState = dbReader.GetDataObjectRel(searchSubScenes);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Subscenes!";
                   return result;
               }

               result.Result.AddRange(dbReader.ObjectRels.OrderBy(scene => scene.OrderID));

               if (result.Result.Any())
               {
                   var subScenesResult = GetSubItems((from subScene in result.Result
                                                      join scene in scenes on subScene.ID_Other equals scene.GUID into scenes2
                                                      from scene in scenes2.DefaultIfEmpty()
                                                      where scene == null
                                                      select subScene).ToList()
                                                     , dbReader);
                   result.ResultState = subScenesResult.ResultState;
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       return result;
                   }
                   result.Result.AddRange(subScenesResult.Result);
               }

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<UseCaseRels>> GetUseCaseRels(List<clsOntologyItem> useCases, OntologyModDBConnector dbReader)
        {
            var taskResult = await Task.Run<ResultItem<UseCaseRels>>(async () =>
            {
                var result = new ResultItem<UseCaseRels>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new UseCaseRels()
                };

                if (!useCases.Any()) return result;

                var searchSubUseCases = useCases.Select(useCase => new clsObjectRel
                {
                    ID_Parent_Object = Config.LocalData.ClassRel_Use_Case_contains_Use_Case.ID_Class_Left,
                    ID_RelationType = Config.LocalData.ClassRel_Use_Case_contains_Use_Case.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Use_Case_contains_Use_Case.ID_Class_Right
                }).ToList();

                result.ResultState = dbReader.GetDataObjectRel(searchSubUseCases);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the sub UseCases!";
                    return result;
                }

                result.Result.AllUseCaseRels = dbReader.ObjectRels;

                result.Result.SubUseCaseRels.AddRange((from useCase in useCases
                                                       join rel in dbReader.ObjectRels on useCase.GUID equals rel.ID_Object
                                                       select rel));

                var subResult = await GetSubUseCaseRels(result.Result.SubUseCaseRels, dbReader);

                result.ResultState = subResult.ResultState;

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }
                
                result.Result.SubUseCaseRels.AddRange(from parentRel in subResult.Result
                                                      join containedRel in result.Result.SubUseCaseRels on new
                                                      {
                                                          parentRel.ID_Object,
                                                          parentRel.ID_RelationType,
                                                          parentRel.ID_Other
                                                      }
                                                                                     equals new
                                                                                     {
                                                                                         containedRel.ID_Object,
                                                                                         containedRel.ID_RelationType,
                                                                                         containedRel.ID_Other
                                                                                     } into containedRels
                                                      from containedRel in containedRels.DefaultIfEmpty()
                                                      where containedRel == null
                                                      select parentRel);

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsObjectRel>>> GetSubUseCaseRels(List<clsObjectRel> parentRelations, OntologyModDBConnector dbReader)
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectRel>>>(async() =>
           {
               var result = new ResultItem<List<clsObjectRel>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new List<clsObjectRel>()
               };

               result.Result.AddRange(from parentRel in parentRelations
                                      join containedRel in result.Result on new { parentRel.ID_Object, 
                                                                                  parentRel.ID_RelationType, 
                                                                                  parentRel.ID_Other } 
                                                                     equals new { containedRel.ID_Object, 
                                                                                  containedRel.ID_RelationType, 
                                                                                  containedRel.ID_Other } into containedRels
                                      from containedRel in containedRels.DefaultIfEmpty()
                                      where containedRel == null
                                      select parentRel);

               var searchSubUseCases = parentRelations.Select(rel => new clsObjectRel
               {
                   ID_Object = rel.ID_Other,
                   ID_RelationType = Config.LocalData.ClassRel_Use_Case_contains_Use_Case.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Use_Case_contains_Use_Case.ID_Class_Right
               }).ToList();

               if (searchSubUseCases.Any())
               {
                   result.ResultState = dbReader.GetDataObjectRel(searchSubUseCases);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the sub UseCases!";
                       return result;
                   }

                   var subUseCaseRels = dbReader.ObjectRels;
                   result.Result.AddRange(subUseCaseRels);
                   var resultSubUseCases = await GetSubUseCaseRels(subUseCaseRels, dbReader);
                   result.ResultState = resultSubUseCases.ResultState;

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       return result;
                   }
                   result.Result.AddRange(from subRel in resultSubUseCases.Result
                                          join containedRel in result.Result on new
                                          {
                                              subRel.ID_Object,
                                              subRel.ID_RelationType,
                                              subRel.ID_Other
                                          }
                                                                         equals new
                                                                         {
                                                                             containedRel.ID_Object,
                                                                             containedRel.ID_RelationType,
                                                                             containedRel.ID_Other
                                                                         } into containedRels
                                          from containedRel in containedRels.DefaultIfEmpty()
                                          where containedRel == null
                                          select subRel);

               }

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<CreateUseCaseGraphMLFileModelResult>> GetCreateUseCaseGraphMLFileModel(CreateUseCaseGraphMLFileRequest request)
        {
            var taskResult = await Task.Run<ResultItem<CreateUseCaseGraphMLFileModelResult>>(() =>
           {
               var result = new ResultItem<CreateUseCaseGraphMLFileModelResult>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new CreateUseCaseGraphMLFileModelResult()
               };

               var dbReaderRootConfig = new OntologyModDBConnector(globals);
               var getRootConfig = dbReaderRootConfig.GetOItem(request.IdConfig, globals.Type_Object);

               if (getRootConfig.GUID_Related == globals.LState_Error.GUID)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "Error while getting the Root-Config!";
                   return result;
               }

               result.Result.RootConfig = getRootConfig;

               var searchSubConfigs = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.RootConfig.GUID,
                       ID_RelationType = Config.LocalData.RelationType_contains.GUID,
                       ID_Parent_Other = Config.LocalData.Class_UseCases_to_GraphML.GUID
                   }
               };

               var dbReaderSubConfigs = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderSubConfigs.GetDataObjectRel(searchSubConfigs);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Sub-Configs!";
                   return result;
               }

               result.Result.Configs = dbReaderSubConfigs.ObjectRels.Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type = rel.Ontology
               }).ToList();

               if (!result.Result.Configs.Any())
               {
                   result.Result.Configs.Add(result.Result.RootConfig);
               }

               var searchExportPaths = result.Result.Configs.Select(config => new clsObjectRel
               {
                   ID_Object = config.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_UseCases_to_GraphML_export_to_Path.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_UseCases_to_GraphML_export_to_Path.ID_Class_Right
               }).ToList();

               var dbReaderPaths = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderPaths.GetDataObjectRel(searchExportPaths);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the export-paths of Configs!";
                   return result;
               }

               result.Result.ConfigsToPaths = dbReaderPaths.ObjectRels;

               if (!result.Result.ConfigsToPaths.Any())
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No export-paths found!";
                   return result;
               }

               var searchResources = result.Result.Configs.Select(config => new clsObjectRel
               {
                   ID_Object = config.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_UseCases_to_GraphML_belonging_Resource.ID_RelationType,
               }).ToList();

               var dbReaderResources = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderResources.GetDataObjectRel(searchResources);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Resources of Configs!";
                   return result;
               }

               result.Result.ConfigsToResources = dbReaderResources.ObjectRels;

               if (!result.Result.ConfigsToResources.Any())
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No Resources of Configs found!";
                   return result;
               }

               return result;
           });

            return taskResult;
        }

        public ResultItem<List<clsObjectRel>> GetSubItems(List<clsObjectRel> parentScenes, OntologyModDBConnector dbReader)
        {
            var result = new ResultItem<List<clsObjectRel>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<clsObjectRel>()
            };

            var searchSubScenes = parentScenes.Select(rel => new clsObjectRel
            {
                ID_Object = rel.ID_Other,
                ID_RelationType = Config.LocalData.ClassRel_Scenes_contains_Scenes.ID_RelationType,
                ID_Parent_Other = Config.LocalData.ClassRel_Scenes_contains_Scenes.ID_Class_Right
            }).ToList();

            result.ResultState = dbReader.GetDataObjectRel(searchSubScenes);

            if (result.ResultState.GUID == globals.LState_Error.GUID)
            {
                result.ResultState.Additional1 = "Error while getting the Subscenes!";
                return result;
            }

            result.Result = new List<clsObjectRel>(dbReader.ObjectRels);
            
            if (result.Result.Any())
            {
                var subScenesResult = GetSubItems((from subScene in result.Result
                                                   join parentScene in parentScenes on subScene.ID_Other equals parentScene.ID_Object into parentScenes2
                                                   from parentScene in parentScenes2.DefaultIfEmpty()
                                                   where parentScene == null
                                                   select subScene).ToList()
                                                   , dbReader);
                result.ResultState = subScenesResult.ResultState;

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.AddRange(subScenesResult.Result.OrderBy(scene => scene.OrderID));
            }

            return result;
        }

        public async Task<ResultItem<clsObjectAtt>> GetNameTransformTemplate()
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<clsObjectAtt>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var searchTemplate = new List<clsObjectAtt>
                {
                    new clsObjectAtt
                    {
                        ID_Object = NameTransform.Config.LocalData.Object_Base_Config__Template_.GUID
                    }
                };

                var dbReaderTemplate = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderTemplate.GetDataObjectAtt(searchTemplate);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the template!";
                    return result;
                }

                result.Result = dbReaderTemplate.ObjAtts.FirstOrDefault();

                return result;
            });
            return taskResult;
        }

        public async Task<ResultItem<ExportUseCasesToJsonModel>> GetExportUseCasesToJsonModel(
            ExportUseCasesToJsonRequest request)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<ExportUseCasesToJsonModel>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new ExportUseCasesToJsonModel()
                };

                var searchConfig = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = request.IdConfig
                    }
                };

                var dbReaderConfig = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfig.GetDataObjects(searchConfig);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Use Cases!";
                    return result;
                }

                result.Result.Config = dbReaderConfig.Objects1.FirstOrDefault();

                result.ResultState = Validation.ValidationController.ValidateExportUseCaseToJsonModel(result.Result,
                    globals, nameof(ExportUseCasesToJsonModel.Config));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchUseCases = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = ExportToJson.Config.dll.LocalData.ClassRel_UseCases_to_Json_contains_Use_Case
                            .ID_RelationType,
                        ID_Parent_Other = ExportToJson.Config.dll.LocalData.ClassRel_UseCases_to_Json_contains_Use_Case
                            .ID_Class_Right
                    }
                };

                var dbReaderUseCases = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderUseCases.GetDataObjectRel(searchUseCases);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Use Cases!";
                    return result;
                }

                result.Result.ConfigToUseCases = dbReaderUseCases.ObjectRels;

                result.ResultState = Validation.ValidationController.ValidateExportUseCaseToJsonModel(result.Result,
                    globals, nameof(ExportUseCasesToJsonModel.ConfigToUseCases));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchPaths = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = ExportToJson.Config.dll.LocalData.ClassRel_UseCases_to_Json_export_to_Path
                            .ID_RelationType,
                        ID_Parent_Other = ExportToJson.Config.dll.LocalData.ClassRel_UseCases_to_Json_export_to_Path
                            .ID_Class_Right
                    }
                };

                var dbReaderPaths = new OntologyModDBConnector(globals);
                result.ResultState = dbReaderPaths.GetDataObjectRel(searchPaths);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Export-Paths!";
                    return result;
                }

                result.Result.ConfigToPaths = dbReaderPaths.ObjectRels;

                result.ResultState = Validation.ValidationController.ValidateExportUseCaseToJsonModel(result.Result,
                    globals, nameof(ExportUseCasesToJsonModel.ConfigToPaths));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                return result;
            });

            return taskResult;

        }

        public ElasticServiceAgent(Globals globals)
        {
            this.globals = globals;
        }
    }
}
