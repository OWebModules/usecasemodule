﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using UseCaseModule.Models;

namespace UseCaseModule.Validation
{
    public static class ValidationController
    {
        public static clsOntologyItem ValidateExportUseCasesToJsonRequest(ExportUseCasesToJsonRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is no valid GUID!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateExportUseCaseToJsonModel(ExportUseCasesToJsonModel model, Globals globals, string propertyName = null)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ExportUseCasesToJsonModel.Config))
            {
                if (model.Config == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No Config provided!";
                    return result;
                }

                if (model.Config.GUID_Parent != ExportToJson.Config.dll.LocalData.Class_UseCases_to_Json.GUID)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "The Config is not of Type UseCase to Json!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ExportUseCasesToJsonModel.ConfigToUseCases))
            {
                if (!model.ConfigToUseCases.Any())
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No Use Cases provided!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ExportUseCasesToJsonModel.ConfigToPaths))
            {
                if (!model.ConfigToPaths.Any())
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No Paths provided!";
                    return result;
                }
            }

            return result;
        }
    }
}
