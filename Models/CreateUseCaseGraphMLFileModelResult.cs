﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UseCaseModule.Models
{
    public class CreateUseCaseGraphMLFileModelResult
    {
        public clsOntologyItem RootConfig { get; set; }
        public List<clsOntologyItem> Configs { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectRel> ConfigsToResources { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> ConfigsToPaths { get; set; } = new List<clsObjectRel>();
    }
}
