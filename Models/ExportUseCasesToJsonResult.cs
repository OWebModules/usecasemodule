﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UseCaseModule.Models
{
    public class ExportUseCasesToJsonResult
    {
        public List<UseCase> UseCases { get; set; }
        public List<string> ExportPaths { get; set; } = new List<string>();
    }
}
