﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UseCaseModule.Models
{
    public class GetUseCaseResourcesResult
    {
        public List<UseCase> UseCases { get; set; } = new List<UseCase>();
        public List<clsObjectRel> Ids { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> Descriptions { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> Actors { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> Conditions { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> Scenes { get; set; } = new List<clsObjectRel>();
    }
}
