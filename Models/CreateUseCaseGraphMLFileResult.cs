﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UseCaseModule.Models
{
    public class CreateUseCaseGraphMLFileResult
    {
        public clsOntologyItem ResultState { get; set; }
        public clsOntologyItem Config { get; set; }
        public string PathCreatedFile { get; set; }
    }
}
