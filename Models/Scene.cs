﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UseCaseModule.Models
{
    public class Scene
    {
        public string IdScene { get; set; }
        public string NameScene { get; set; }
        public List<Scene> SubScenes { get; set; } = new List<Scene>();
        public Scene ParentScene { get; set; }
        public bool IsMainScene { get; set; }

        public string IdUseCase { get; set; }

        public long OrderId { get; set; }

    }
}
