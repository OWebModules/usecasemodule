﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UseCaseModule.Models
{
    public class GetUseCaseListResult
    {
        public clsOntologyItem RefItem { get; set; }

        public List<UseCase> UseCases { get; set; } = new List<UseCase>();
        public List<UseCase> AllUseCases { get; set; } = new List<UseCase>();

        public List<clsObjectRel> UseCaseRels { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> ParentRels { get; set; } = new List<clsObjectRel>();
    }
}
