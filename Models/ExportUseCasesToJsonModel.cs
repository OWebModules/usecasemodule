﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntologyClasses.BaseClasses;

namespace UseCaseModule.Models
{
    public class ExportUseCasesToJsonModel
    {
        public clsOntologyItem Config { get; set; }
        public List<clsObjectRel> ConfigToUseCases { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> ConfigToPaths { get; set; } = new List<clsObjectRel>();
    }
}
