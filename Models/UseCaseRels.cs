﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UseCaseModule.Models
{
    public class UseCaseRels
    {
        public List<clsOntologyItem> UseCases { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectRel> AllUseCaseRels { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> SubUseCaseRels { get; set; } = new List<clsObjectRel>();
    }
}
