﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OntologyAppDBConnector;

namespace UseCaseModule.Models
{
    public class ExportUseCasesToJsonRequest
    {
        public string IdConfig { get; }

        public CancellationToken CancellationToken { get; }
        public IMessageOutput MessageOutput { get; set; }

        public ExportUseCasesToJsonRequest(string idConfig, CancellationToken cancellationToken)
        {
            IdConfig = idConfig;
            CancellationToken = cancellationToken;
        }

        
    }
}
