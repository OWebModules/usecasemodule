﻿using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UseCaseModule.Models
{
    public class CreateUseCaseGraphMLFileRequest
    {
        public string IdConfig { get; private set; }

        public OntologyAppDBConnector.IMessageOutput MessageOutput { get; set; }

        public CreateUseCaseGraphMLFileRequest(string idConfig)
        {
            IdConfig = idConfig;
        }
    }
}
