﻿using GraphMLConnector.Models;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UseCaseModule.Models
{
    public class UseCase
    {
        public string IdUseCase { get; set; }
        public string NameUseCase { get; set; }
        public string UseCasePath { get; set; }

        public string UseCaseExplicitId { get; set; }

        public UseCase ParentUseCase { get; set; }

        public List<UseCase> SubUseCases { get; set; } = new List<UseCase>();

        public NodeItem NodeItem { get; set; }

        public clsObjectRel UseCaseToDescription { get; set; }
        public clsObjectAtt HtmlDescription { get; set; }
        public List<clsOntologyItem> PrimaryActors { get; set; } = new List<clsOntologyItem>();
        public List<clsOntologyItem> SecondaryActors { get; set; } = new List<clsOntologyItem>();

        public List<clsOntologyItem> PreConditions { get; set; } = new List<clsOntologyItem>();
        public List<clsOntologyItem> PostConditions { get; set; } = new List<clsOntologyItem>();

        public List<Scene> MainScenes { get; set; } = new List<Scene>();
        public List<Scene> AdditionalScenes { get; set; } = new List<Scene>();


        public List<UseCase> GetAllSubUseCases()
        {
            var result = new List<UseCase>();
            result.AddRange(SubUseCases);
            foreach (var useCase in SubUseCases)
            {
                result.AddRange(useCase.GetAllSubUseCases());
            }

            return result;
        }

        public bool IsParentOrSelf(string idUseCase)
        {
            if (IdUseCase == idUseCase) return true;
            if (ParentUseCase == null) return false;
            return ParentUseCase.IsParentOrSelf(idUseCase);
        }

    }
}
