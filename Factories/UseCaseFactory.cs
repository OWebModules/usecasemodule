﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UseCaseModule.Models;

namespace UseCaseModule.Factories
{
    public class UseCaseFactory
    {
        private Globals globals;
        public async Task<ResultItem<List<UseCase>>> GetUseCases(List<clsOntologyItem> useCaseList, List<clsObjectRel> subUseCaseRels, List<clsObjectRel> allUseCaseRels)
        {
            var taskResult = await Task.Run<ResultItem<List<UseCase>>>(() =>
           {
               var result = new ResultItem<List<UseCase>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new List<UseCase>()
               };

               var wholeList = useCaseList.GroupBy(useCase => new { useCase.GUID, useCase.Name }).Select(useCase => new UseCase
               {
                   IdUseCase = useCase.Key.GUID,
                   NameUseCase = useCase.Key.Name,
                   UseCasePath = GetUseCasePath(useCase.Key.GUID, allUseCaseRels)
               }).ToList();

               wholeList.AddRange((from left in subUseCaseRels
                                   join useCaseExist in wholeList on left.ID_Object equals useCaseExist.IdUseCase into useCaseExists
                                  from useCaseExist in useCaseExists.DefaultIfEmpty()
                                  where useCaseExist == null
                                   select left).GroupBy(left => new {left.ID_Object, left.Name_Object}).Select(grp => new UseCase
                                  {
                                      IdUseCase = grp.Key.ID_Object,
                                      NameUseCase = grp.Key.Name_Object,
                                       UseCasePath = GetUseCasePath(grp.Key.ID_Object, allUseCaseRels)
                                   }));

               wholeList.AddRange((from right in subUseCaseRels
                                   join useCaseExist in wholeList on right.ID_Other equals useCaseExist.IdUseCase into useCaseExists
                                  from useCaseExist in useCaseExists.DefaultIfEmpty()
                                  where useCaseExist == null
                                  select right).GroupBy(right => new {right.ID_Other, right.Name_Other}).Select(grp => new UseCase
                                  {
                                      IdUseCase = grp.Key.ID_Other,
                                      NameUseCase = grp.Key.Name_Other,
                                      UseCasePath = GetUseCasePath(grp.Key.ID_Other, allUseCaseRels)
                                  }));

               result.Result.AddRange(from useCase in wholeList
                                      join useCaseRel in subUseCaseRels on useCase.IdUseCase equals useCaseRel.ID_Other into useCaseRels1
                                      from useCaseRel in useCaseRels1.DefaultIfEmpty()
                                      where useCaseRel == null
                                      select useCase);

               var subResult = GetSubUseCases(result.Result, wholeList, subUseCaseRels);
               result.ResultState = subResult.ResultState;

               return result;
           });

            return taskResult;
        }

        private string GetUseCasePath(string idUseCase, List<clsObjectRel> useCaseRels)
        {
            var path = "";
            var rel = useCaseRels.FirstOrDefault(relU => relU.ID_Other == idUseCase);
            if (rel != null)
            {
                path = rel.Name_Object;
                var parPath = GetUseCasePath(rel.ID_Object, useCaseRels);
                if (!string.IsNullOrEmpty(parPath))
                {
                    path = $@"{parPath}\{path}";
                }
            }
            return path;
        }

        private ResultItem<List<UseCase>> GetSubUseCases(List<UseCase> parentUseCases, List<UseCase> allUseCases, List<clsObjectRel> useCaseRels)
        {
            var result = new ResultItem<List<UseCase>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = allUseCases
            };

            if (!parentUseCases.Any()) return result;

            foreach (var useCase in parentUseCases)
            {
                useCase.SubUseCases = useCaseRels.Where(rel => rel.ID_Object == useCase.IdUseCase).Select(rel => new UseCase
                {
                    IdUseCase = rel.ID_Other,
                    NameUseCase = rel.Name_Other,
                    ParentUseCase = useCase,
                }).ToList();

                var subCasesToAnalyze = (from subUseCase in useCase.SubUseCases
                                         join rel in useCaseRels on subUseCase.IdUseCase equals rel.ID_Object
                                         select new { subUseCase, rel }).Where(sub => !sub.subUseCase.IsParentOrSelf(sub.rel.ID_Other)).Select(sub =>
                                           sub.subUseCase).ToList();
                if (subCasesToAnalyze.Any())
                {
                    var subResult = GetSubUseCases(subCasesToAnalyze, allUseCases, useCaseRels);

                    result.ResultState = subResult.ResultState;

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

            }

            return result;
        }

        public UseCaseFactory(Globals globals)
        {
            this.globals = globals;
        }
    }
}
