﻿using GraphMLConnector.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using UseCaseModule.Models;

namespace UseCaseModule.Factories
{
    public class NodeEdgeFactory
    {
        private Globals globals;

        public async Task<ResultItem<NodeEdgePackage>> CreateNodeEdgePackage(List<UseCase> useCases, List<GroupItem> groupItems, List<clsObjectRel> useCaseRels)
        {
            var taskResult = await Task.Run<ResultItem<NodeEdgePackage>>(() =>
           {
               var result = new ResultItem<NodeEdgePackage>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new NodeEdgePackage
                   {
                       GroupItems = groupItems
                   }
               };
               var allGroupItems = groupItems.SelectMany(groupItem => groupItem.GetAllSubGroupItems()).ToList();
               allGroupItems.AddRange(groupItems);

               var nonGroupItems = from useCase in useCases
                                   join groupItem in allGroupItems on useCase.IdUseCase equals groupItem.GroupId into groupItems1
                                   from groupItem in groupItems1.DefaultIfEmpty()
                                   where groupItem == null
                                   select useCase;

               var priPres = nonGroupItems.SelectMany(useCase => useCase.PrimaryActors).GroupBy(act => new { act.GUID, act.Name }).Select(actGrp => new NodeItem
               {
                   IdNode = $"pri_{actGrp.Key.GUID}",
                   NameNode = HttpUtility.HtmlEncode(actGrp.Key.Name),
                   SVGSourceId = 1,
                   XmlTemplate = Config.LocalData.Object_Graphml___UML__Primary_Actor
               }).ToList();

               var secPres = nonGroupItems.SelectMany(useCase => useCase.SecondaryActors).GroupBy(act => new { act.GUID, act.Name }).Select(actGrp => new NodeItem
               {
                   IdNode = $"sec_{actGrp.Key.GUID}",
                   NameNode = HttpUtility.HtmlEncode(actGrp.Key.Name),
                   SVGSourceId = 1,
                   XmlTemplate = Config.LocalData.Object_Graphml___UML__Primary_Actor
               }).ToList();

               foreach (var useCase in nonGroupItems)
               {
                   useCase.NodeItem = new NodeItem
                   {
                       IdNode = useCase.IdUseCase,
                       NameNode = HttpUtility.HtmlEncode(useCase.NameUseCase),
                       XmlTemplate = Config.LocalData.Object_Graphml___UML__Use_Case
                   };

                   var primaryNodes = (from priAct in useCase.PrimaryActors
                                       join priPre in priPres on $"pri_{priAct.GUID}" equals priPre.IdNode
                                       select priPre).ToList();
                   
                   var secondaryNodes = (from secAct in useCase.SecondaryActors
                                         join secPri in priPres on $"sec_{secAct.GUID}" equals secPri.IdNode
                                         select secPri).ToList();

                   result.Result.EdgeItems.AddRange(primaryNodes.Select(node => new EdgeItem
                   {
                       NodeItem2 = useCase.NodeItem,
                       NodeItem1 = node,
                       RelationType = Config.LocalData.RelationType_Primary,
                       XmlTemplate = Config.LocalData.Object_Graphml___UML_Edge_without_Label
                   }));

                   result.Result.EdgeItems.AddRange(secondaryNodes.Select(node => new EdgeItem
                   {
                       NodeItem2 = useCase.NodeItem,
                       NodeItem1 = node,
                       RelationType = Config.LocalData.RelationType_Primary,
                       XmlTemplate = Config.LocalData.Object_Graphml___UML_Edge_without_Label
                   }));

                   result.Result.NodeItems.Add(useCase.NodeItem);
                   result.Result.NodeItems.AddRange(from pri in primaryNodes
                                                    join priIn in result.Result.NodeItems on pri.IdNode equals priIn.IdNode into priIns
                                                    from priIn in priIns.DefaultIfEmpty()
                                                    where priIn == null
                                                    select pri);
                   result.Result.NodeItems.AddRange(from sec in secondaryNodes
                                                    join secIn in result.Result.NodeItems on sec.IdNode equals secIn.IdNode into secIns
                                                    from priIn in secIns.DefaultIfEmpty()
                                                    where priIn == null
                                                    select sec);
               }

               foreach (var groupItem in allGroupItems)
               {
                   groupItem.Nodes = (from useCaseRel in useCaseRels.Where(rel => rel.ID_Object == groupItem.GroupId)
                                      join useCase in nonGroupItems on useCaseRel.ID_Other equals useCase.IdUseCase
                                      select useCase.NodeItem).ToList();

                   groupItem.Edges = (from node2 in groupItem.Nodes
                                      join edge in result.Result.EdgeItems on node2.IdNode equals edge.NodeItem2.IdNode
                                      join node1 in result.Result.NodeItems on edge.NodeItem1.IdNode equals node1.IdNode
                                      select edge).ToList();

                   groupItem.Nodes.AddRange(groupItem.Edges.Select(edge => edge.NodeItem1));
                   groupItem.Nodes.AddRange(groupItem.Edges.Select(edge => edge.NodeItem2));
               }

               
               return result;
           });

            return taskResult;
        }

        public NodeEdgeFactory(Globals globals)
        {
            this.globals = globals;
        }
    }
}
