﻿using GraphMLConnector.Models;
using HtmlEditorModule;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GraphMLConnector.Validation;
using TypedTaggingModule.Connectors;
using UseCaseModule.Factories;
using UseCaseModule.Models;

namespace UseCaseModule
{
    public class UseCaseConnector : AppController
    {
        public async Task<ResultItem<GetUseCaseListResult>> GetUseCaseListByTypedTag(GetUseCaseListRequestTypedTag request)
        {
            var taskResult = await Task.Run<ResultItem<GetUseCaseListResult>>(async() =>
           {
               var result = new ResultItem<GetUseCaseListResult>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new GetUseCaseListResult()
               };

               request.MessageOutput?.OutputInfo("Validating request...");
               if (string.IsNullOrEmpty(request.IdRefItem))
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "IdRefItem is empty!";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               if (!Globals.is_GUID(request.IdRefItem))
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "IdRefItem is no valid Guid!";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }
               request.MessageOutput?.OutputInfo("Validated request");

               var dbReader = new OntologyModDBConnector(Globals);

               request.MessageOutput?.OutputInfo("Get Reference...");
               var refItem = dbReader.GetOItem(request.IdRefItem, Globals.Type_Object);

               if (refItem.GUID_Related == Globals.LState_Error.GUID)
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "Error while getting the RefItem!";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }
               request.MessageOutput?.OutputInfo($"Have Reference:{refItem.Name}");

               request.MessageOutput?.OutputInfo($"Get Tags...");
               var typedTaggingController = new TypedTaggingConnector(Globals);

               var typedTags = await typedTaggingController.GetTags(refItem);

               result.ResultState = typedTags.Result;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Typed Tags!";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo($"Have {typedTags.TypedTags.Count} Typed Tags.");

               var useCases = typedTags.TypedTags.Where(tag => tag.IdTagParent == Config.LocalData.Class_Use_Case.GUID).Select(tag => new clsOntologyItem
               {
                   GUID = tag.IdTag,
                   Name = tag.NameTag,
                   GUID_Parent = tag.IdTagParent,
                   Type = Globals.Type_Object
               }).ToList();

               request.MessageOutput?.OutputInfo($"Have {useCases.Count} Use Cases.");

               var useCaseResult = await GetUseCaseList(useCases, refItem, request.MessageOutput);
               result.ResultState = useCaseResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<GetUseCaseListResult>> GetUseCaseList(List<clsOntologyItem> useCases, clsOntologyItem refItem, IMessageOutput messageOutput)
        {
            var taskResult = await Task.Run(async() =>
            {
                var result = new ResultItem<GetUseCaseListResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new GetUseCaseListResult()
                };


                if (!useCases.Any())
                {
                    messageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                var elasticAgent = new Services.ElasticServiceAgent(Globals);

                var getUseCaseRels = await elasticAgent.GetUseCaseRels(useCases, new OntologyModDBConnector(Globals));

                result.ResultState = getUseCaseRels.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    messageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                result.Result.UseCaseRels = getUseCaseRels.Result.SubUseCaseRels;

                var factory = new UseCaseFactory(Globals);

                var useCasesResult = await factory.GetUseCases(useCases, getUseCaseRels.Result.SubUseCaseRels, getUseCaseRels.Result.AllUseCaseRels);

                result.ResultState = useCasesResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    messageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                var htmlEditorController = new HtmlEditorConnector(Globals);
                result.Result.AllUseCases = useCasesResult.Result.SelectMany(useCase => useCase.GetAllSubUseCases()).ToList();
                result.Result.AllUseCases.AddRange(useCasesResult.Result);
                messageOutput?.OutputInfo($"Get Resources of Use Cases...");
                var resourcesResult = await elasticAgent.GetuseCaseResources(result.Result.AllUseCases);

                result.ResultState = resourcesResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    messageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                messageOutput?.OutputInfo($"Have Resources of Use Cases.");

                messageOutput?.OutputInfo($"Get Descriptions...");
                var htmlDescriptions = new List<ConnectorResultHtmlDocument>();
                if (resourcesResult.Result.Descriptions.Any())
                {
                    var htmlDescriptionsResult = await htmlEditorController.GetHtmlDocuments(resourcesResult.Result.Descriptions.Select(desc => desc.ID_Other).ToList());
                    result.ResultState = htmlDescriptionsResult.ResultState;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        messageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                    htmlDescriptions = htmlDescriptionsResult.Result;
                }

                messageOutput?.OutputInfo($"Get SceenesTree of {resourcesResult.Result.Scenes.Count} Scenes");
                var getScenesTreeResult = await elasticAgent.GetScenesTree(resourcesResult.Result.Scenes.OrderBy(scene => scene.OrderID).Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).ToList());

                result.ResultState = getScenesTreeResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    messageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                var resultScenesTree = GetScenesList(resourcesResult.Result, getScenesTreeResult.Result);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    messageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                messageOutput?.OutputInfo($"Created SceenesTree.");

                result.Result.RefItem = refItem;

                result.Result.UseCases = useCasesResult.Result;

                messageOutput?.OutputInfo($"Enrich {result.Result.AllUseCases.Count} Use Cases...");
                foreach (var useCase in result.Result.AllUseCases)
                {
                    useCase.UseCaseExplicitId = resourcesResult.Result.Ids.FirstOrDefault(id => id.ID_Object == useCase.IdUseCase)?.Name_Other ?? useCase.IdUseCase;
                    useCase.UseCaseToDescription = resourcesResult.Result.Descriptions.FirstOrDefault(desc => desc.ID_Object == useCase.IdUseCase);
                    if (useCase.UseCaseToDescription != null)
                    {
                        var htmlItem = htmlDescriptions.FirstOrDefault(html => html.OItemRef.GUID == useCase.UseCaseToDescription.ID_Other);
                        if (htmlItem != null)
                        {
                            useCase.HtmlDescription = htmlItem.OAItemHtmlDocument;
                        }
                    }
                    useCase.PrimaryActors = resourcesResult.Result.Actors.Where(act => act.ID_RelationType == Config.LocalData.ClassRel_Use_Case_Primary_Actor.ID_RelationType && act.ID_Object == useCase.IdUseCase).OrderBy(rel => rel.OrderID).ThenBy(rel => rel.Name_Other).Select(rel => new clsOntologyItem
                    {
                        GUID = rel.ID_Other,
                        Name = rel.Name_Other,
                        GUID_Parent = rel.ID_Parent_Other,
                        Type = rel.Ontology
                    }).ToList();

                    useCase.SecondaryActors = resourcesResult.Result.Actors.Where(act => act.ID_RelationType == Config.LocalData.ClassRel_Use_Case_Secondary_Actor.ID_RelationType && act.ID_Object == useCase.IdUseCase).OrderBy(rel => rel.OrderID).ThenBy(rel => rel.Name_Other).Select(rel => new clsOntologyItem
                    {
                        GUID = rel.ID_Other,
                        Name = rel.Name_Other,
                        GUID_Parent = rel.ID_Parent_Other,
                        Type = rel.Ontology
                    }).ToList();

                    useCase.PreConditions = resourcesResult.Result.Conditions.Where(act => act.ID_RelationType == Config.LocalData.ClassRel_Use_Case_Pre_Conditions.ID_RelationType && act.ID_Object == useCase.IdUseCase).OrderBy(rel => rel.OrderID).ThenBy(rel => rel.Name_Other).Select(rel => new clsOntologyItem
                    {
                        GUID = rel.ID_Other,
                        Name = rel.Name_Other,
                        GUID_Parent = rel.ID_Parent_Other,
                        Type = rel.Ontology
                    }).ToList();

                    useCase.PostConditions = resourcesResult.Result.Conditions.Where(act => act.ID_RelationType == Config.LocalData.ClassRel_Use_Case_Posts_Conditions.ID_RelationType && act.ID_Object == useCase.IdUseCase).OrderBy(rel => rel.OrderID).ThenBy(rel => rel.Name_Other).Select(rel => new clsOntologyItem
                    {
                        GUID = rel.ID_Other,
                        Name = rel.Name_Other,
                        GUID_Parent = rel.ID_Parent_Other,
                        Type = rel.Ontology
                    }).ToList();

                    useCase.MainScenes = resultScenesTree.Result.Where(scene => scene.IsMainScene && scene.IdUseCase == useCase.IdUseCase).ToList();
                    useCase.AdditionalScenes = resultScenesTree.Result.Where(scene => !scene.IsMainScene && scene.IdUseCase == useCase.IdUseCase).ToList();
                }
                messageOutput?.OutputInfo($"Enriched {result.Result.AllUseCases.Count} Use Cases.");

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<CreateUseCaseGraphMLFileResult>>> CreateUseCaseGraphMLFile(CreateUseCaseGraphMLFileRequest request)
        {
            var taskResult = await Task.Run<ResultItem<List<CreateUseCaseGraphMLFileResult>>>(async() =>
           {
               var result = new ResultItem<List<CreateUseCaseGraphMLFileResult>>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new List<CreateUseCaseGraphMLFileResult>()
               };

               var elasticAgent = new Services.ElasticServiceAgent(Globals);
               
               request.MessageOutput?.OutputInfo("Get model...");
               var modelResult = await elasticAgent.GetCreateUseCaseGraphMLFileModel(request);

               result.ResultState = modelResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo("Habve model.");

               foreach (var config in modelResult.Result.Configs)
               {
                   request.MessageOutput?.OutputInfo($"Config: {config.Name}");

                   var resultState = new CreateUseCaseGraphMLFileResult
                   {
                       ResultState = Globals.LState_Success.Clone(),
                       Config = config
                   };
                   result.Result.Add(resultState);

                   var resourceRel = modelResult.Result.ConfigsToResources.SingleOrDefault(rel => rel.ID_Object == config.GUID);
                   if (resourceRel == null)
                   {
                       resultState.ResultState = Globals.LState_Error.Clone();
                       resultState.ResultState.Additional1 = $"No resource for config \"{config.GUID}\" found!";
                       continue;
                   }

                   request.MessageOutput?.OutputInfo($"Resource: {resourceRel.Name_Other}");

                   var pathRel = modelResult.Result.ConfigsToPaths.SingleOrDefault(rel => rel.ID_Object == config.GUID);
                   if (pathRel == null)
                   {
                       resultState.ResultState = Globals.LState_Error.Clone();
                       resultState.ResultState.Additional1 = $"No export-path for config \"{config.GUID}\" found!";
                       continue;
                   }

                   request.MessageOutput?.OutputInfo($"Path: {pathRel.Name_Other}");

                   var getUseCases = await GetUseCaseListByTypedTag(new GetUseCaseListRequestTypedTag(resourceRel.ID_Other) { MessageOutput = request.MessageOutput });

                   result.ResultState = getUseCases.ResultState;

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);

                       return result;
                   }



                   if (string.IsNullOrEmpty(pathRel.Name_Other))
                   {
                       result.ResultState = Globals.LState_Error.Clone();
                       result.ResultState.Additional1 = $"The file-path {pathRel.Name_Other} is not valid!";
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }

                   request.MessageOutput?.OutputInfo($"Get Group Items of {getUseCases.Result.AllUseCases.Count} Use Cases...");
                   var groupItemsResult = GetGroupItems(getUseCases.Result.AllUseCases);
                   result.ResultState = groupItemsResult.ResultState;

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }
                   request.MessageOutput?.OutputInfo($"Have Group Items of {getUseCases.Result.AllUseCases.Count} Use Cases.");

                   var nodeEdgeFactory = new NodeEdgeFactory(Globals);

                   request.MessageOutput?.OutputInfo($"Create Node and Edge package of {getUseCases.Result.AllUseCases.Count} Use Cases, {groupItemsResult.Result.Count} Group-Items, {getUseCases.Result.UseCaseRels.Count} Use Case Relations...");
                   var nodeEdgePackage = await nodeEdgeFactory.CreateNodeEdgePackage(getUseCases.Result.AllUseCases, groupItemsResult.Result, getUseCases.Result.UseCaseRels);

                   result.ResultState = nodeEdgePackage.ResultState;

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }

                   request.MessageOutput?.OutputInfo($"Created Node and Edge package of {getUseCases.Result.AllUseCases.Count} Use Cases, {groupItemsResult.Result.Count} Group-Items, {getUseCases.Result.UseCaseRels.Count} Use Case Relations.");

                   var graphMLConnector = new GraphMLConnector.GraphMLConnector(Globals);

                   var requestCreateGraphMLFile = new GraphMLConnector.Models.ExportGraphRequest
                   {
                       FileName = pathRel.Name_Other,
                       GroupItems = nodeEdgePackage.Result.GroupItems,
                       Nodes = nodeEdgePackage.Result.NodeItems,
                       Edges = nodeEdgePackage.Result.EdgeItems,
                       SvgSources = new List<clsOntologyItem> { Config.LocalData.Object_Graphml___UML__Actor_Source },
                       MessageOutput = request.MessageOutput
                   };

                   var createResult = await graphMLConnector.ExportGraph(requestCreateGraphMLFile);
                   resultState.ResultState = createResult;
                   resultState.PathCreatedFile = pathRel.Name_Other;
               }

               if (result.Result.All(res => res.ResultState.GUID == Globals.LState_Error.GUID))
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No path was successful exported!";
                   return result;
               }
               return result;
           });

            return taskResult;
        }

        private ResultItem<List<GroupItem>> GetGroupItems(List<UseCase> useCases, GroupItem parentGroupItem = null)
        {
            var result = new ResultItem<List<GroupItem>>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new List<GroupItem>()
            };

            var useCasesToGroupItems = useCases.Where(useCase => parentGroupItem == null ? useCase.ParentUseCase == null : useCase.ParentUseCase != null && useCase.ParentUseCase.IdUseCase == parentGroupItem.GroupId);

            var groupBase = useCasesToGroupItems.Where(useCase => useCase.SubUseCases.Any());

            result.Result = groupBase.Select(useCase => new GroupItem
            {
                GroupId = useCase.IdUseCase,
                GroupName = useCase.NameUseCase
            }).ToList();

            var subGroupItems = new List<GroupItem>();
            foreach (var groupItem in result.Result)
            {
                var getSubGroupItems = GetGroupItems(useCases, groupItem);
                result.ResultState = getSubGroupItems.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }
                groupItem.GroupItems = getSubGroupItems.Result;
            }

            return result;
        }

        private ResultItem<List<Scene>> GetScenesList(GetUseCaseResourcesResult resources, List<clsObjectRel> subScenes)
        {
            var result = new ResultItem<List<Scene>>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new List<Scene>()
            };

            var rootScenes = resources.Scenes.Select(sceneItm => new Scene
            {
                IdScene = sceneItm.ID_Other,
                NameScene = sceneItm.Name_Other,
                IsMainScene = sceneItm.ID_RelationType == Config.LocalData.ClassRel_Use_Case_Main_Scenes.ID_RelationType,
                IdUseCase = sceneItm.ID_Object,
                OrderId = sceneItm.OrderID.Value
            }).OrderBy(scene => scene.NameScene).ThenBy(scene => scene.OrderId).ToList();

            var subScenesResult = GetSubScenes(rootScenes, subScenes);

            result.ResultState = subScenesResult.ResultState;

            result.Result = rootScenes;

            return result;
        }

        private ResultItem<List<Scene>> GetSubScenes(List<Scene> parentScenes, List<clsObjectRel> subScenesRaw)
        {
            var result = new ResultItem<List<Scene>>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new List<Scene>()
            };


            var subScenes = (from parentScene in parentScenes
                             join subScene in subScenesRaw on parentScene.IdScene equals subScene.ID_Object
                             select new Scene
                             {
                                 IdScene = subScene.ID_Other,
                                 NameScene = subScene.Name_Other,
                                 ParentScene = parentScene,
                                 OrderId = subScene.OrderID.Value
                             }).ToList();

            parentScenes.ForEach(parentScene =>
            {
                parentScene.SubScenes = subScenes.Where(subScene => subScene.ParentScene == parentScene).OrderBy(scene => scene.OrderId).ThenBy(scene => scene.NameScene).ToList();
            });

            if (subScenes.Any())
            {
                var subScenesResult = GetSubScenes(subScenes, subScenesRaw);

                result.ResultState = subScenesResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }
            }


            result.Result = parentScenes;
            return result;
        }

        public async Task<ResultItem<ExportUseCasesToJsonResult>> ExportUseCasesToJson(
            ExportUseCasesToJsonRequest request)
        {
            var taskResult = await Task.Run(async () =>
            {
                var result = new ResultItem<ExportUseCasesToJsonResult>
                {
                    ResultState =  Globals.LState_Success.Clone(),
                    Result = new ExportUseCasesToJsonResult()
                };

                request.MessageOutput?.OutputInfo("Validate request...");
                result.ResultState =
                    Validation.ValidationController.ValidateExportUseCasesToJsonRequest(request, Globals);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Validated request.");

                request.MessageOutput?.OutputInfo("Get Model...");

                var serviceAgent = new Services.ElasticServiceAgent(Globals);
                var modelResult = await serviceAgent.GetExportUseCasesToJsonModel(request);

                result.ResultState = modelResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Have Model.");

                if (request.CancellationToken.IsCancellationRequested)
                {
                    request.MessageOutput?.OutputInfo("User abort!");
                    return result;
                }
                result.Result.ExportPaths = modelResult.Result.ConfigToPaths.Select(path => path.Name_Other).ToList();
                request.MessageOutput?.OutputInfo($"Export to: {string.Join("\r\n",result.Result.ExportPaths)}");

                var useCaseList = modelResult.Result.ConfigToUseCases.Select(res => new clsOntologyItem
                {
                    GUID = res.ID_Other,
                    Name = res.Name_Other,
                    GUID_Parent = res.ID_Parent_Other,
                    Type = res.Ontology
                }).ToList();

                request.MessageOutput?.OutputInfo($"Export {useCaseList.Count} UseCases...");

                var useCasesResult = await GetUseCaseList(useCaseList, null, request.MessageOutput);
                result.ResultState = useCasesResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                try
                {
                    using (var streamWriter = new StreamWriter(modelResult.Result.ConfigToPaths.First().Name_Other))
                    {
                        var sbJson = new StringBuilder();
                        sbJson.Append("{ \"UseCases\": [");
                        request.MessageOutput?.OutputInfo($"Export UseCases...");
                        for (var i = 0; i < useCasesResult.Result.AllUseCases.Count; i++)
                        {
                            if (i > 0)
                            {
                                sbJson.Append(", ");
                            }
                            if (request.CancellationToken.IsCancellationRequested)
                            {
                                request.MessageOutput?.OutputInfo("User abort!");
                                return result;
                            }
                            var useCase = useCasesResult.Result.AllUseCases[i];

                            sbJson.Append("{ \"GUID\":" + Newtonsoft.Json.JsonConvert.SerializeObject( useCase.IdUseCase ) +  ",\"Name\": " + Newtonsoft.Json.JsonConvert.SerializeObject( useCase.NameUseCase ));

                            if (useCase.PrimaryActors.Any())
                            {
                                sbJson.Append($", \"{nameof(useCase.PrimaryActors)}\":");
                                var actorsJson = Newtonsoft.Json.JsonConvert.SerializeObject(useCase.PrimaryActors);
                                sbJson.Append(actorsJson);
                            }

                            if (useCase.SecondaryActors.Any())
                            {
                                sbJson.Append($", \"{nameof(useCase.SecondaryActors)}\":");
                                var actorsJson = Newtonsoft.Json.JsonConvert.SerializeObject(useCase.PrimaryActors);
                                sbJson.Append(actorsJson);
                            }

                            if (useCase.HtmlDescription != null)
                            {
                                sbJson.Append($", \"{nameof(useCase.HtmlDescription)}\":");
                                sbJson.Append(Newtonsoft.Json.JsonConvert.SerializeObject( useCase.HtmlDescription.Val_String ));
                            }

                            if (useCase.PreConditions.Any())
                            {
                                sbJson.Append($", \"{nameof(useCase.PreConditions)}\":");
                                var actorsJson = Newtonsoft.Json.JsonConvert.SerializeObject(useCase.PreConditions);
                                sbJson.Append(actorsJson);
                            }

                            if (useCase.PostConditions.Any())
                            {
                                sbJson.Append($", \"{nameof(useCase.PostConditions)}\":");
                                var actorsJson = Newtonsoft.Json.JsonConvert.SerializeObject(useCase.PostConditions);
                                sbJson.Append(actorsJson);
                            }

                            if (useCase.MainScenes.Any())
                            {
                                sbJson.Append($", \"{nameof(useCase.MainScenes)}\": [");
                                AddScenes(useCase.MainScenes, sbJson);
                                sbJson.Append("]");
                            }

                            if (useCase.AdditionalScenes.Any())
                            {
                                sbJson.Append($", \"{nameof(useCase.AdditionalScenes)}\": [");
                                AddScenes(useCase.MainScenes, sbJson);
                                sbJson.Append("]");
                            }


                            sbJson.Append("}");

                            if (i % 20 == 0)
                            {
                                streamWriter.Write(sbJson.ToString());
                                sbJson.Clear();
                                request.MessageOutput?.OutputInfo($"Exported {i} Use Cases");
                            }
                        }
                        sbJson.Append("] }");
                        streamWriter.Write(sbJson);
                    }

                    request.MessageOutput?.OutputInfo($"Exported UseCases.");
                }
                catch (Exception e)
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = e.Message;
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                
                
                
                return result;
            });

            return taskResult;
        }

        private void AddScenes(List<Scene> scenes, StringBuilder sbJson)
        {
            for (var i = 0; i < scenes.Count; i++)
            {
                var scene = scenes[i];
                if (i > 0)
                {
                    sbJson.AppendLine(", ");
                }
                sbJson.Append("{");
                sbJson.Append($"\"Name\": \"{ scene.NameScene }\"");

                if (scene.SubScenes.Any())
                {
                    sbJson.Append(", \"SubScenes\" :[");
                    AddScenes(scene.SubScenes, sbJson);
                    sbJson.Append("]");
                }
                sbJson.Append("}");
            }
        }

        public UseCaseConnector(Globals globals) : base(globals)
        {
        }
    }
}
