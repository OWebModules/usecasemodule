﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using UseCaseModule.Factories;
using UseCaseModule.Models;
using UseCaseModule.Services;

namespace UseCaseModule
{
    public class UseCaseNameTransformController : INameTransform
    {
        private Globals globals;

        public bool IsReferenceCompatible => false;

        public bool IsAspNetProject { get; set; }

        public async Task<ResultItem<List<clsOntologyItem>>> TransformNames(List<clsOntologyItem> items, bool encodeName = true, string idClass = null, IMessageOutput messageOutput = null)
        {
            var taskResult = await Task.Run(async () =>
           {
               var result = new ResultItem<List<clsOntologyItem>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = items
               };

               messageOutput?.OutputInfo("Validate items...");

               if (!items.Any())
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No items provided!";
                   messageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               if (!items.All(itm => itm.GUID_Parent == Config.LocalData.Class_Use_Case.GUID))
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = $"Not all items are of class {Config.LocalData.Class_Use_Case.Name}!";
                   messageOutput?.OutputError(result.ResultState.Additional1);
                   return result;

               }

               messageOutput?.OutputInfo("Validated items.");

               var useCaseConnector = new UseCaseConnector(globals);
               var elasticAgent = new ElasticServiceAgent(globals);

               messageOutput?.OutputInfo("Get template...");
               var templateResult = await elasticAgent.GetNameTransformTemplate();
               result.ResultState = templateResult.ResultState;

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   messageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               var template = Properties.Settings.Default.DefaultTemplate;
               if (templateResult.Result != null)
               {
                   template = templateResult.Result.Val_String;
                   messageOutput?.OutputInfo("Found template.");
               }
               else
               {
                   messageOutput?.OutputInfo("Take default template.");
               }

               var useCaseResult = await useCaseConnector.GetUseCaseList(items, null, messageOutput);
               result.ResultState = useCaseResult.ResultState;

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   messageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               foreach (var useCase in (from usec in useCaseResult.Result.UseCases
                                        join item in items on usec.IdUseCase equals item.GUID
                                        select new { usec, item }))
               {
                   var resultText = template;

                   resultText = resultText.Replace("@PATH@", useCase.usec.UseCasePath ?? "");
                   resultText = resultText.Replace("@NAME@", useCase.usec.NameUseCase);
                   resultText = resultText.Replace("@ID@", useCase.usec.UseCaseExplicitId);
                   var actorHtml = "";
                   if (useCase.usec.PrimaryActors.Any())
                   {
                       actorHtml = "<ol>";
                       foreach (var actor in useCase.usec.PrimaryActors)
                       {
                           actorHtml += $"<li>{HttpUtility.HtmlEncode(actor.Name)}</li>";
                       }
                       actorHtml += "</ol>";
                       resultText = resultText.Replace("@PRI_ACTORS@", actorHtml);
                   }

                   var descriptionHtml = "";
                   if (useCase.usec.HtmlDescription != null)
                   {
                       descriptionHtml = $"<a class=\"ontologyItem Content\" name=\"{useCase.usec.HtmlDescription.ID_Object}\"></a>";
                   }
                   else if (useCase.usec.UseCaseToDescription != null)
                   {
                       descriptionHtml = useCase.usec.UseCaseToDescription.Name_Other;
                   }

                   resultText = resultText.Replace("@DESCRIPTION@", descriptionHtml);

                   actorHtml = "";

                   if (useCase.usec.SecondaryActors.Any())
                   {
                       actorHtml = "<ol>";
                       foreach (var actor in useCase.usec.SecondaryActors)
                       {
                           actorHtml += $"<li>{HttpUtility.HtmlEncode(actor.Name)}</li>";
                       }
                       actorHtml += "</ol>";
                   }

                   resultText = resultText.Replace("@SEC_ACTORS@", actorHtml);

                   var conditionHtml = "";
                   if (useCase.usec.PreConditions.Any())
                   {
                       conditionHtml = "<ol>";
                       foreach (var condition in useCase.usec.PreConditions)
                       {
                           conditionHtml += $"<li>{HttpUtility.HtmlEncode(condition.Name)}</li>";
                       }
                       conditionHtml += "</ol>";
                   }
                   
                   resultText = resultText.Replace("@PRE_CONDITIONS@", conditionHtml);

                   conditionHtml = "";
                   if (useCase.usec.PostConditions.Any())
                   {
                       conditionHtml = "<ol>";
                       foreach (var condition in useCase.usec.PostConditions)
                       {
                           conditionHtml += $"<li>{HttpUtility.HtmlEncode(condition.Name)}</li>";
                       }
                       conditionHtml += "</ol>";
                   }
                   
                   resultText = resultText.Replace("@POST_CONDITIONS@", conditionHtml);

                   var mainScenesHtml = GetScenesHtml(useCase.usec.MainScenes);
                   resultText = resultText.Replace("@MAIN_SCENES@", mainScenesHtml);

                   var additionalScenesHtml = GetScenesHtml(useCase.usec.AdditionalScenes);
                   resultText = resultText.Replace("@ADDITIONAL_SCENES@", additionalScenesHtml);

                   if (encodeName)
                   {
                       useCase.item.Val_String = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(resultText));
                   }
                   else
                   {
                       useCase.item.Name = resultText;
                   }
               }

               return result;
           });

            return taskResult;
        }

        public string GetScenesHtml(List<Scene> scenes)
        {
            if (!scenes.Any())
            {
                return "";
            }
            
            var result = "<ol>";

            foreach (var scene in scenes.OrderBy(scene => scene.OrderId))
            {
                result += $"<li>{HttpUtility.HtmlEncode(scene.NameScene)}";
                var subSceneResult = GetScenesHtml(scene.SubScenes);
                if (!string.IsNullOrEmpty(subSceneResult))
                {
                    result += subSceneResult;
                }
                result += "</li>";
            }

            result += "</ol>";
            return result;
        }

        public bool IsResponsible(string idClass)
        {
            return idClass == Config.LocalData.Class_Use_Case.GUID;
        }

        public UseCaseNameTransformController(Globals globals)
        {
            this.globals = globals;
        }
    }
}
